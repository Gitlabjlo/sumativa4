/* Programa que calcula el promedio de ventas semanal para tres vendedores de la NASA*/
#include <stdio.h> 


int main()
{
	float ventas[3][5];
	float proventas;
	float sumaventas;
	int i;
	int j;
	ventas[0][0];
	ventas[0][1];
	ventas[0][2];
	ventas[0][3];
	ventas[0][4];
	ventas[1][0];
	ventas[1][1];
	ventas[1][2];
	ventas[1][3];
	ventas[1][4];
	ventas[2][0];
	ventas[2][1];
	ventas[2][2];
	ventas[2][3];
	ventas[2][4];
	
	for (i = 0; i < 3; i++){
		
		proventas = 0;
		sumaventas = 0;
		
		for (j=0;j<5;j++){
			
			printf ("Introduzca las ventas para el vendedor  %.f\n", i);
			scanf ("%f", &ventas[i][j]);
			sumaventas = sumaventas + ventas[i][j];
		}
		proventas= (sumaventas/5.0);
    	printf("El promedio de ventas semanal de la NASA para el vendedor %d es: %.2f\n ", i, proventas);
    	
	
	}
			
	return 0;
	
}
